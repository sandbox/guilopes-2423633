<?php
/**
 * @file
 * Module file for Image SVG formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function image_svg_formatter_field_formatter_info() {
  $formatters = array(
    'image_svg_formatter' => array(
      'label' => t('Image SVG Formatter'),
      'description' => t('Image SVG Formatter'),
      'field types' => array('image'),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function image_svg_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'image_svg_formatter':
      // Each elements.
      foreach ($items as $delta => $file) {
        // Validate svg files.
        if (!preg_match('%\.(svg)$%i', $file['uri'])) {
          return FALSE;
        }

        // Get SVG Element.
        $result = image_svg_formatter_get_content($file['uri'], $file['fid']);
        if ($result) {
          // Render result.
          $element[$delta] = array(
            '#type' => 'markup',
            '#markup' => $result,
          );
        }
      }
      break;
  }

  return $element;
}

/**
 * Get SVG By URI.
 *
 * @return string
 *   SVG element.
 */
function image_svg_formatter_get_content($uri, $fid) {
  $cid = 'image_svg_formatter:' . $fid;
  $data = &drupal_static($cid);

  if (!isset($data)) {
    if ($cache = cache_get($cid)) {
      $data = $cache->data;
    }
    else {
      $data = FALSE;

      if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
        $file_url = $wrapper->getExternalUrl();

        // Exit each.
        if (isset($file_url)) {
          // Custom for amazon WS.
          $file_url = preg_replace('/^(\/\/)/', 'http://', $file_url);

          $ctx = stream_context_create(array('http'=> array('timeout' => 1,)));
          $file_contents = @file_get_contents($file_url, FALSE, $ctx);

          // If dont has content return false.
          if (!empty($file_contents)) {
            // Remove XML.
            $svg = explode('?xml', $file_contents);
            if (count($svg) > 1) {
              $svg = explode('?>', $svg[1]);
              $result = $svg[1];
            }
            else {
              $result = $svg[0];
            }

            // Remove DOCTYPE if exists.
            $svg = explode('<!DOCTYPE', $result);
            if (count($svg) > 1) {
              $svg = explode('>', $svg[1]);
              // Remove primeiro elemento.
              unset($svg[0]);
              $data = implode('>', $svg);
            }
            else {
              $data = $svg[0];
            }
          }
        }
      }

      cache_set($cid, $data, 'cache');
    }
  }

  return $data;
}

